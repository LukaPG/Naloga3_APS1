
import java.util.Arrays;
import java.util.Scanner;

class EStack{
    private int maxSize;
    private int[][] stackArray;
    private int top;

    public EStack(int maxSize) {
        this.maxSize = maxSize;
        stackArray = new int[this.maxSize][2];
        top = -1;
    }

    public void push(int[] element) {
        stackArray[++top] = element;
    }

    public int[] pop() {
        return stackArray[top--];
    }

    public int[] peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }

    public int[][] getEdges(){
        if(isFull()){
            return stackArray;
        }
        return new int[1][1];
    }

    public void clear() {
        stackArray = new int[maxSize][2];
        top = -1;
    }
}

class Stack {
    private int maxSize;
    private int[] stackArray;
    private int top;

    public Stack(int maxSize) {
        this.maxSize = maxSize;
        stackArray = new int[this.maxSize];
        top = -1;
    }

    public void push(int element) {
        stackArray[++top] = element;
    }

    public int pop() {
        return stackArray[top--];
    }

    public int peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == maxSize - 1);
    }

    public void clear() {
        stackArray = new int[maxSize];
        top = -1;
    }
    public void print(){
        for(int e : stackArray){
            System.out.printf("%d ", e);
        }
        System.out.printf("%n");
    }
}

class Queue{
    private int[] elements;
    private int index;
    private int size;

    public Queue(int size){
        elements = new int[size];
        this.size = size;
    }
    public boolean isEmpty(){
        return index==0;
    }

    public void enqueue(int element){

        this.elements[index]=element;
        this.index++;
    }

    public int dequeue(){
        int element=this.elements[0];
        for(int i =0; i<this.index-1; i++){
            elements[i]=elements[i+1];
        }
        this.index--;
        return element;

    }
}

public class Naloga3 {

    static boolean directed;
    static int[][] edges;
    static int numOfVert;
    static Stack entry;
    static int[] exit;
    static boolean[] discovered;



    static int[][] mulMatrix(int[][] m1, int[][] m2){
        int[][] out = new int[m1.length][m2[0].length];

        for(int i = 0; i<out.length; i++){
            for (int j = 0; j<out[0].length; j++){
                int sum = 0;
                for (int k = 0; k<m2.length; k++){
                    sum += m1[i][k]*m2[k][j];
                }
                out[i][j] = sum;
            }
        }
        return out;
    }

    static int[][] edgesAdjToVert(int vertex){
        int[][] result = new int[edges.length][2];
        int i = 0;

        for(int[] edge : edges){
            if(edge[0] == vertex || edge[1] == vertex){
                result[i++] = edge;
            }
        }
        int[][] ret = new int[i][2];
        for(int j = 0; j<i; j++){
            ret[j] = result[j];
        }
        return ret;
    }

    static int[] adjVertices(int vertex){
        int[] temp = new int[numOfVert-1];
        int i=0;

        if (directed) {
            for (int[] edge : edges) {
                if (edge[0] == vertex) {
                    temp[i++] = edge[1];
                }
            }
        }else{
            for (int[] edge : edges) {
                if (edge[0] == vertex) {
                    temp[i++] = edge[1];
                }else if(edge[1] == vertex){
                    temp[i++] = edge[0];
                }
            }
        }

        int[] ret = new int[i];
        for (int j = 0; j<i; j++){
            ret[j] = temp[j];
        }
        return selectionSort(ret); //TODO check if works
    }

    static int[][] walks(int k){
        //calculate number of k long walks in graph
        /*
        make adjacency matrix
        A^k
        where there remains a 1, there is a walk
        count number of 1s in the final matrix
         */
        int[][] adjMatrix = new int[numOfVert][numOfVert];
        for(int[] edge : edges){
            adjMatrix[edge[0]][edge[1]] = 1;
            if (! directed) adjMatrix[edge[1]][edge[0]] = 1;
        }
        int[][] origMatrix = adjMatrix;


        for(int i = 1; i<k; i++){
            adjMatrix = mulMatrix(adjMatrix, origMatrix);
        }

        return adjMatrix;
    }

    static void dfs(int vert) {
        if (!discovered[vert]) {
            discovered[vert] = true;
            entry.push(vert);
        }

        for (int adjacent : adjVertices(vert)) {
            if (!discovered[adjacent]) {
                dfs(adjacent);
            }
        }

        for(int j=0; j<exit.length; j++){
            if(exit[j]==-1){
                exit[j]=vert;
                break;
            }
        }
    }

    static int time;
    static int[] disc;
    static int[] dist;

    static void BFS(int vert, String[] args){
        Queue q = new Queue(numOfVert);
        time += 1;
        disc[vert] = time;
        q.enqueue(vert);
        while(! q.isEmpty()){
            vert = q.dequeue();
            if (args[1].equals("bfs")) System.out.print(vert+" ");

            for (int adjacent : adjVertices(vert)){
                if (disc[adjacent] == 0){
                    time += 1;
                    disc[adjacent] = time;
                    q.enqueue(adjacent);
                    dist[adjacent] = dist[vert]+1;
                }
            }
        }
    }

    static void comp(){
        if(!directed) {
            for (int i = 0; i < numOfVert; i++) {
                if (!discovered[i]) {
                    DFSUtil(i);
                    int[] out = new int[counter];
                    for (int j = 0; j < counter; j++) {
                        out[j] = components[j];
                    }
                    out = selectionSort(out);
                    for (int e : out) {
                        System.out.printf("%d ", e);
                    }
                    System.out.printf("%n");
                    counter = 0;
                    components = new int[numOfVert];

                }
            }
        }else{
            index = 0;
            idx = 0;
            S = new Stack(numOfVert);
            indexes = new int[numOfVert];
            low = new int[numOfVert];
            onStack = new boolean[numOfVert];
            dir_components = new int[numOfVert][numOfVert];
            for(int b=0; b<numOfVert; b++){
                indexes[b]=-1;
                low[b]=-1;
                onStack[b]=false;
            }

            for (int i = 0; i<numOfVert; i++){
                if (indexes[i] == -1){
                    strongConnect(i);
                }
            }

            Arrays.sort(dir_components, (a, b) -> Integer.compare(a[0], b[0]));
            for(int[] comp : dir_components){
                comp = selectionSort(comp);
                if(! arrayEmpty(comp)) {
                    for (int e : comp) {
                        System.out.printf("%d ", e);
                    }
                    System.out.println();
                }
            }
        }
    }

    static boolean isSafe(int vert, int[][] adjMat, int path[], int pos) {
        /* Check if this vertex is an adjacent vertex of
           the previously added vertex.
        int[][] adjMatrix = new int[numOfVert][numOfVert];
        for(int[] edge : edges){
            adjMatrix[edge[0]][edge[1]] = 1;
            if (! directed) adjMatrix[edge[1]][edge[0]] = 1;
        }
*/

        if (adjMat[path[pos-1]][vert] == 0)
            return false;

        // Check if the vertex has already been included.
        for (int i = 0; i < pos; i++)
            if (path[i] == vert)
                return false;

        return true;
    }

    static boolean hamCycleUtil(int adjMat[][], int path[], int pos){
        /* base case: If all vertices are included in
           Hamiltonian Cycle */

        if (pos==numOfVert) return true;

        // Try different vertices as a next candidate in
        // Hamiltonian Cycle. We don't try for 0 as we
        // included 0 as starting point in in hamCycle()
        for (int v = 1; v < numOfVert; v++){
            /* Check if this vertex can be added to Hamiltonian
               Cycle */
            if (isSafe(v, adjMat, path, pos)) {
                path[pos] = v;

                /* recur to construct rest of the path */
                if (hamCycleUtil(adjMat, path, pos + 1)) return true;

                /* If adding vertex v doesn't lead to a solution,
                   then remove it */
                path[pos] = -1;
            }
        }

        /* If no vertex can be added to Hamiltonian Cycle
           constructed so far, then return false */
        return false;
    }

    static int[] path;

    static int hamCycle(int adjMat[][]) {
        path = new int[numOfVert];
        for (int i = 0; i < numOfVert; i++)
            path[i] = -1;

        /* Let us put vertex 0 as the first vertex in the path.
           If there is a Hamiltonian Cycle, then the path can be
           started from any point of the cycle as the graph is
           undirected */
        path[0] = 0;
        if (! hamCycleUtil(adjMat, path, 1))
        {
            System.out.println("\nSolution does not exist");
            return 0;
        }

        for(int e : path){
            System.out.printf("%d ", e);
        }
        System.out.println();
        return 1;
    }

    static int[][] generateAdjMat(){
        int count = 0;
        int[][] con_comps = new int[numOfVert][numOfVert];

        for (int i = 0; i < numOfVert; i++) {
            if (!discovered[i]) {
                DFSUtil(i);
                int[] out = new int[counter];
                for (int j = 0; j < counter; j++) {
                    out[j] = components[j];
                }
                con_comps[count] = selectionSort(out);
                count++;
                counter = 0;
                components = new int[numOfVert];
            }
        }
        //add edge (component[0], component[-1])

        EStack S_edges = new EStack(edges.length+count);
        for (int[] edge : edges){
            S_edges.push(edge);
        }
        for (int i=0; i<count; i++){
            int[] edge = new int[2];
            if (i == count-1){
                edge[0] = con_comps[i][0];
                edge[1] = con_comps[0][con_comps[0].length-1];
            }else {
                edge[0] = con_comps[i][0];
                edge[1] = con_comps[i + 1][con_comps[i + 1].length - 1];
            }
            S_edges.push(edge);
        }

        int[][] edges_arr = S_edges.getEdges();

        int[][] adjMatrix = new int[numOfVert][numOfVert];
        for(int[] edge : edges_arr) {
            adjMatrix[edge[0]][edge[1]] = 1;
            adjMatrix[edge[1]][edge[0]] = 1;
        }
        return adjMatrix;
    }

    static boolean arrayEmpty(int[] arr){
        boolean a = true;
        for (int e : arr){
            if (e != 0) a = false;
        }
        return a;
    }
    static int index;
    static int idx;
    static Stack S;
    static int[] indexes;
    static int[] low;
    static boolean[] onStack;
    static int[][] dir_components;

    static void strongConnect(int vertex){
        indexes[vertex] = index;
        low[vertex] = index;
        index+=1;
        S.push(vertex);
        onStack[vertex] = true;

        for(int[] edge : edgesAdjToVert(vertex)){
            if (edge[0] == vertex){
                if(indexes[edge[1]] == -1){
                    strongConnect(edge[1]);
                    low[vertex] = Math.min(low[vertex], low[edge[1]]);
                }else if(onStack[edge[1]]){
                    low[vertex] = Math.min(low[vertex], indexes[edge[1]]);
                }
            }
        }

        if(indexes[vertex] == low[vertex]) {
            int w;
            int[] component_tmp = new int[numOfVert];
            int i = 0;
            do {
                w = S.pop();
                onStack[w] = false;
                component_tmp[i++] = w;
            } while (vertex != w);

            int[] component = new int[i];
            for (int a = 0; a<i; a++){
                component[a]=component_tmp[a];
            }
            component = selectionSort(component);
            dir_components[idx++] = component;

        }

    }

    static int[] components;
    static int counter;

    static void DFSUtil(int vert){
        discovered[vert] = true;
        components[counter++]=vert;
        for (int adjacent : adjVertices(vert)){
            if(!discovered[adjacent]){
                DFSUtil(adjacent);
            }
        }
    }

    static int[] selectionSort(int[] arr){

        for (int i=0;i<arr.length-1;i++){
            int index = i;
            for (int j=i+1;j<arr.length;j++)
                if (arr[j] < arr[index])
                    index = j;

            int smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }




    public static void main(String[] args) {

        directed = args[0].equals("directed");

        Scanner sc = new Scanner(System.in);
        numOfVert = Integer.parseInt(sc.nextLine().trim());
        entry = new Stack(numOfVert);
        exit  = new int[numOfVert];
        for(int e=0; e<numOfVert; e++){
            exit[e]=-1;
        }

        time  = 0;
        disc  = new int[numOfVert];
        dist  = new int[numOfVert];
        components = new int[numOfVert];
        counter = 0;

        discovered = new boolean[numOfVert];
        for(boolean e : discovered){
            e = false;
        }
        int[][] temp;
        if (directed){
            temp = new int[numOfVert*(numOfVert-1)][2];
        }
        else{
            temp = new int[(numOfVert*(numOfVert-1))/2][2];
        }

        int i = 0;
        while (sc.hasNext()){
            String link = sc.nextLine();
            String[] nums = link.split(" ");
            temp[i][0] = Integer.parseInt(nums[0]);
            temp[i][1] = Integer.parseInt(nums[1]);
            i++;
        }

        edges = new int[i][2];
        for (int j=0; j<i; j++){
            edges[j][0]   = temp[j][0];
            edges[j][1] = temp[j][1];
        }

        switch (args[1]){
            case "walks":
                int k = Integer.parseInt(args[2]);
                int[][] adjMat = walks(k);
                for (int[] row : adjMat){
                    for (int num : row){
                        System.out.printf("%d ", num);
                    }
                    System.out.printf("%n");
                }
                break;
            case "dfs":
                for(int vertex = 0; vertex<numOfVert; vertex++){
                    if(!discovered[vertex]) dfs(vertex);
                }
                entry.print();
                for(int e : exit){
                    System.out.printf("%d ", e);
                }
                System.out.printf("%n");
                break;

            case "bfs":
                //System.out.printf("0");
                for(i=0; i<disc.length; i++){
                    if (disc[i] == 0){
                        BFS(i, args);
                    }
                }
                break;
            case "sp":
                int source = Integer.parseInt(args[2]);
                BFS(source, args);
                for(i = 0; i<numOfVert; i++){
                    if (disc[i]==0){
                        System.out.printf("%d %d%n", i, -1);
                    }else {
                        System.out.printf("%d %d%n", i, dist[i]);
                    }
                }
                break;
            case "comp":
                comp();
                break;
            case "ham":
                hamCycle(generateAdjMat());
                break;
        }
    }
}
